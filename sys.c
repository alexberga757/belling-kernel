

void print_at(const char* str, int row, int col)
{
    char *vidptr = (char*)0xb8000;
    unsigned int i = (row * 80 + col) * 2;
    
    while (*str != '\0') {
        vidptr[i] = *str;
        vidptr[i + 1] = 0x07;  // Attribute-byte: black background, light grey foreground
        ++str;
        i += 2;
    }
}

void clear() {
    print(' ');
}

void print(const char* str) {
        /* video memory begins at address 0xb8000 */
    char *vidptr = (char*)0xb8000;
    unsigned int i = 0;
    unsigned int j = 0;
    unsigned int screensize;

    /* this loops clears the screen
    * there are 25 lines each of 80 columns; each element takes 2 bytes */
    screensize = 80 * 25 * 2;
    while (j < screensize) {
        /* blank character */
        vidptr[j] = ' ';
        /* attribute-byte */
        vidptr[j+1] = 0x07;
        j = j + 2;
    }

    j = 0;

    /* this loop writes the string to video memory */
    while (str[j] != '\0') {
        /* the character's ascii */
        vidptr[i] = str[j];
        /* attribute-byte: give character black bg and light grey fg */
        vidptr[i+1] = 0x07;
        ++j;
        i = i + 2;
    }
}

void print_c(const char* str, unsigned char fg_color, unsigned char bg_color)
{
    // Set initial position to (0, 0)
    int row = 0;
    int col = 0;

    char *vidptr = (char*)0xb8000;
    unsigned int i = (row * 80 + col) * 2;
    
    while (*str != '\0') {
        vidptr[i] = *str;
        vidptr[i + 1] = (bg_color << 4) | (fg_color & 0x0F);
        ++str;
        i += 2;
    }
}

void sleep(unsigned int milliseconds)
{
    // In a real kernel, you would interact with hardware timers and interrupts
    // This is a simple demonstration using busy waiting
    for (unsigned int i = 0; i < milliseconds * 100000; ++i) {
        // This loop simulates waiting
        // In a real kernel, you might check a timer or handle interrupts
    }
}



