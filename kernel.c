//sys.c
extern void print_at(const char* str, int row, int col);
extern void print(const char* str);
extern void print_c(const char* str, unsigned char fg_color, unsigned char bg_color);
extern void clear();
extern void sleep(unsigned int milliseconds);

//halt.c
extern void reboot();
extern void shutdown();

void kmain(void)
{
    int col_h = 0;
    for (int i = 0;i < 20;i++) {
        print_at("HI!",col_h,0);
 
        sleep(500);
        col_h++;
    }
    reboot();
}



