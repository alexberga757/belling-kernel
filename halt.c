void shutdown() {
    // x86 assembly for ACPI power-off
    asm volatile (
        "mov $0x2401, %%ax\n"   // ACPI Power Off
        "mov $0x0, %%bx\n"      // Not used, must be zero
        "int $0x15\n"            // Call BIOS interrupt
        :
        : 
        : "%ax", "%bx"
    );

    // If the above assembly fails, you might want to add error handling or halt the CPU.
}

void reboot() {
    // x86 assembly for system reboot
    asm volatile (
        "mov $0x1234, %%ax\n"   // Magic value for warm reboot
        "int $0x16\n"            // Call BIOS interrupt
        :
        :
        : "%ax"
    );

    // If the above assembly fails, you might want to add error handling or halt the CPU.
}