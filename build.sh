#!/bin/bash
rm -fr ./output
nasm -f elf32 kernel.asm -o kernel.o
gcc -m32 -c kernel.c -o main.o
gcc -m32 -c sys.c -o sys.o
gcc -m32 -c halt.c -o halt.o
ld -m elf_i386 -T link.ld -o kernel kernel.o main.o sys.o halt.o
mkdir output
mv kernel ./output
rm *.o
